app.controller("SampleController",function(){
	this.name = 'Sample';
	this.persons = outlaws;
	this.isAlive = function(val){
		if(this.persons[val].alive === true){
			return true;
		}
		else{
			return false;
		}
	}
	
});

var outlaws = [
	{
		name:"Billy the Kid",
		reward:4000,
		wanted:"Dead or Alive",
		alive: true
	},
	{
		name:"Wild Bill",
		reward:10000,
		wanted:"Alive",
		alive: false
	},
	{
		name:"Shady Steve",
		reward:3000,
		wanted:"Alive",
		alive: true
	}
] 

app.controller("MenuController",function(){
	this.name = 'onename'
});